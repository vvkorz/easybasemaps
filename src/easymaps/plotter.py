#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
A collection of plotting functions.

"""
import itertools
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

class Mapper:
    """
    Main class to plot some preconfigured maps.
    """

    # Get lat and lng of Germany's main cities.
    top_cities = {
        'Berlin': (13.404954, 52.520008),
        'Cologne': (6.953101, 50.935173),
        'Düsseldorf': (6.782048, 51.227144),
        'Frankfurt am Main': (8.682127, 50.110924),
        'Hamburg': (9.993682, 53.551086),
        'Leipzig': (12.387772, 51.343479),
        'Munich': (11.576124, 48.137154),
        'Dortmund': (7.468554, 51.513400),
        'Stuttgart': (9.181332, 48.777128),
        'Nuremberg': (11.077438, 49.449820),
        'Hannover': (9.73322, 52.37052)
    }

    def __init__(self, plz_shape_df):
        self.plz_shape_df = plz_shape_df


    def plot_german_map(
            self,
            initdata: pd.DataFrame,
            numeric_column_name: str,
            filter_tpl: tuple,
            n_of_bins=20,
            cmap='Reds',
            alpha=0.9,
            initdata_postcode_col_name="postcode",
            shapefiles_postcode_col_name="plz",
            groupby_method="count"
    ):
        """

        .. warning::

            This expects a geopandas Dataframe with German postcodes (`download here <https://www.suche-postleitzahl.org/downloads>`_) see README

            .. code-block:: python

                import geopandas as gpd

                plz_shape_df = gpd.read_file("/path/to/plz-gebiete.shp", dtype={'plz': str})

        Here are a few examples how to use the function:

        >>> mapper = Mapper(plz_shape_df)
        >>> mapper.plot_german_map(
        >>>     initdata,
        >>>     numeric_column_name="signups",
        >>>     filter_tpl = (
        >>>                 ("sales_channel", "EON.DE"),
        >>>                 ("order_year", 2020),
        >>>             ),
        >>>     cmap='Reds',
        >>>     alpha=0.9
        >>> )

        Another example with analytics table

        .. code-block:: python

            import pandas as pd
            import geopandas as gpd
            from easymaps.plotter import Mapper

            analytics_table = pd.read_csv(
                "analytics_strom_p_eonstrom.csv.gz",
                encoding="utf-8",
                compression="gzip",
                sep=",",
                dtype = {"postcode": str}
            )

            plz_shape_df = gpd.read_file("/path/to/plz-gebiete.shp", dtype={'plz': str})

            mp = Mapper(plz_shape_df)

            mp.plot_german_map(
                analytics_table,
                numeric_column_name="total_price",
                filter_tpl = (
                            ("product_name", "E.ON Strom"),
                            ("consumption", 2500)
                        ),
                n_of_bins=10,
                cmap='Reds',
                alpha=0.9,
                initdata_postcode_col_name="postcode",
                groupby_method="mean"  # note mean instead of "count"
            )

        .. note::

            Available groupby methods are `count`, `mean` and `sum`


        :param initdata: Dataframe with initial data
        :type initdata: pandas.DataFrame
        :param numeric_column_name: Column name that contains numeric values that should be plotted
        :type numeric_column_name: str
        :param filter_tpl: a tuple with column names and their values in case initial data should be filtered
        :type filter_tpl: tuple
        :param n_of_bins: number of bins in which the numeric_column should be split when choosing a color for heat map
        :type n_of_bins: int
        :param cmap: matplotlib color map
        :type n_of_bins: str
        :param alpha: matplotlib alpha
        :type alpha: float
        :param initdata_postcode_col_name: column name with postcodes in input df (default postcode)
        :type initdata_postcode_col_name: str
        :param shapefiles_postcode_col_name: column name with postcodes in shapefiels df (default plz)
        :type shapefiles_postcode_col_name: str
        :returns: plotted map
        :rtype: matplotlib.plot object
        """

        assert groupby_method in ("count", "mean", "sum"), "can only work with 'mean', 'count' and 'sum'"

        data = self._strip_data(
            initdata,
            filter_tpl
        )
        grouped = self._extract_postcode(data, numeric_column_name, plz_cname=initdata_postcode_col_name, groupby_method=groupby_method)

        bins, names = self._get_bins(grouped[numeric_column_name], n_of_bins)
        grouped = grouped.assign(agg_numeric_column=pd.cut(grouped[numeric_column_name], bins, labels=names))
        merged = pd.merge(
            self.plz_shape_df,
            grouped,
            left_on=shapefiles_postcode_col_name,
            right_on=initdata_postcode_col_name
        )

        self._plot_map(
            merged,
            numeric_column_name="agg_numeric_column",
            title=f"{numeric_column_name} in Germany. {', '.join(list(map(lambda x: f'{x[0]} = {x[1]}', filter_tpl)))}",
            cmap = cmap,
            alpha = alpha
        )

    def _plot_map(
            self,
            merged: pd.DataFrame,
            numeric_column_name: str,
            title: str,
            cmap='Reds',
            alpha=0.9
    ):
        """
        The actual plotting function
        """
        plt.style.use('seaborn')
        plt.rcParams['figure.figsize'] = [20, 15]

        fig, ax = plt.subplots()

        merged.plot(
            ax=ax,
            column=numeric_column_name,
            categorical=True,
            legend=True,
            legend_kwds={'title': numeric_column_name, 'loc': 'lower right'},
            cmap=cmap,
            alpha=alpha
        )

        for c in self.top_cities.keys():
            ax.text(
                x=self.top_cities[c][0],
                y=self.top_cities[c][1] + 0.08,
                s=c,
                fontsize=12,
                ha='center',
            )

            ax.plot(
                self.top_cities[c][0],
                self.top_cities[c][1],
                marker='o',
                c='black',
                alpha=0.5
            )

        ax.set(
            title=title,
            aspect=1.3,
            facecolor='white'
        )
        plt.show()


    def _get_bins(self, data: iter, chunks: int) -> tuple:
        """
        # get_bins(merged.signups)

        # bins = [0, 2, 18, 35, 65, np.inf]
        # names = ['<2', '2-18', '18-35', '35-65', '65+']
        bins, names = get_bins(grouped.signups, 20)
        # df['AgeRange'] = pd.cut(df['Age'], bins, labels=names)

        grouped = grouped.assign(agg_signups=pd.cut(grouped['signups'], bins, labels=names))
        grouped.tail()
        """
        # get the range of sign ups per postcode
        range_list = sorted(set(data))

        # devide into chunks
        chunksls = list(
            itertools.islice(
                range_list,
                0,
                int(max(range_list)),
                int(len(range_list) / chunks)
            )
        )

        # insert leading 0
        if chunksls[0] != 0:
            chunksls.insert(0, 0)

        chunksls = np.round(np.array(chunksls), 2)

        a = range(1, len(chunksls))

        names = list(map(lambda x: f"{chunksls[x[0]]} - {chunksls[x[1]]}", list(zip(np.array(a) - 1, a))))

        return chunksls, names

    def _extract_postcode(
            self, data: pd.DataFrame,
            numeric_column_name: str,
            plz_cname="postcode",
            groupby_method="count"
    ) -> pd.DataFrame:
        """
        grouped = _extract_postcode(data)
        grouped.head()
        """
        temp = data[
            [
                plz_cname,
                numeric_column_name
            ]
        ]
        kwargs = {plz_cname: temp[plz_cname].astype(str).str.strip(".0")}
        temp = temp.assign(**kwargs)
        kwargs = {plz_cname: temp[plz_cname].str.zfill(5)}
        temp = temp.assign(**kwargs)
        if groupby_method == "count":
            return pd.DataFrame(temp.groupby(plz_cname).count()).reset_index()
        elif groupby_method == "mean":
            return pd.DataFrame(temp.groupby(plz_cname).mean()).reset_index()
        elif groupby_method == "sum":
            return pd.DataFrame(temp.groupby(plz_cname).sum()).reset_index()

    def _strip_data(self, data: pd.DataFrame, filters: tuple) -> pd.DataFrame:
        """
        data = _strip_data(
            initdata,
            (
                ("sales_channel", channel_),
                ("order_year", year_),
            )
        )
        data.sales_channel.unique()
        """
        for column, value in filters:
            data = data[data[column] == value]
        return data

