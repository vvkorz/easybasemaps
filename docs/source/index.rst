easymaps project documentation
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   about
   usage
   source



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
