About
======

easymaps is a simple collection of wrapper functions for ipyleaflet that aims to simplify the creation of maps during your data exploration.

Source code can be found in the following `repository <>`_.

.. warning::

   For now all documentation is contained in the README of the repository.


.. code-block:: console

   $ python
   >>> import antigravity


:Authors:
    Vladimir Korzinov

:Date:
    |today|
