#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Setup file to install automated scripts in your environment
All packages will be installed to python.site-packages
simply run:

    >>> python setup.py install

For a local installation or if you like to develop further

    >>> python setup.py develop --user

"""
import codecs
import sys
import os
import re
from setuptools import setup, find_packages

source_path = "src"
path = os.path.abspath(os.path.dirname(sys.argv[0]))
os.chdir(path)
packages = find_packages(source_path)


def get_version_git():
    # The full version, including alpha/beta/rc tags.
    release = re.sub("^v", "", os.popen("git describe --tags HEAD").read().strip())
    # The short X.Y version.
    return release


def read_files(*filenames):
    """
    Output the contents of one or more files to a single concatenated string.
    """
    output = []
    for filename in filenames:
        f = codecs.open(filename, encoding="utf-8")
        try:
            output.append(f.read())
        finally:
            f.close()
    return "\n\n".join(output)


setup(
    name="easymaps",
    version=get_version_git(),
    description="easymaps",
    long_description=read_files("README.md"),
    author="Vladimir Korzinov",
    author_email="kozinovvv@gmail.com",
    url="",
    packages=packages,
    install_requires=[
        "pandas",
        "matplotlib",
        "ipyleaflet",
        "geopandas",
        "descartes"
    ],
    extras_require={"about-page": ["pip-licenses>=1.7.1"]},
    package_dir={"": source_path},
    zip_safe=False,
    include_package_data=True,
    classifiers=[
        "Development Status :: 0 - PreAlpha",
        "Intended Audience :: Developers",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.7",
        "Framework :: Framework Independent",
    ],
)
