# easymaps

## Documentation

easymaps code is based on the following [blog post](https://juanitorduz.github.io/germany_plots/)

Full documentation can be found on [sphinx docs](https://www.google.com)

# Installation

Install easymaps in your virtual environment like so:

``` bash
$ pip install easybasemaps 
```

# Usage

In order to use the functions you need to get postcode shapefiles of Germany. 
Go ahead and download `plz-gebiete.shp.zip from [official source](https://www.suche-postleitzahl.org/downloads)

You now need to read this files in GeoPandas dataframe:

``` python
import geopandas as gpd
plz_shape_df = gpd.read_file("geodata/plz-gebiete.shp", dtype={'plz': str})
plz_shape_df.head()
```

![plot](docs/source/_static/plz-gebiete-gpd.png)

After that you can use the package like so:

``` python
from easymaps.plotter import Mapper
mp = Mapper(plz_shape_df)
mp.plot_german_map(
    initdata,
    numeric_column_name="signups",
    filter_tpmap_example.pngl = (
                ("sales_channel", channel_),
                ("order_year", 2020),
            ),
    n_of_bins=10,
    cmap='Reds',
    alpha=0.9
)
```
![plot](docs/source/_static/map_example.png)

Please see documentation for more examples.

# Authors

 * vvkorz 
